This repo is a work in progress type definition file for libsodium-wrappers-sumo.

It is not published on npm or @types at this time.

# Usage

For now - install it by copying libsodium.d.ts into your project's source.

# Testing

Simply run `tsc libsodium-types-test.ts`. If it compiles - then the types are at least consistent.
