import * as sodium from "libsodium-wrappers-sumo"; 

const plainText = "hello";
const password = "hunter2";
const salt: Uint8Array = new Uint8Array(10);
const typedNonce: Uint8Array = new Uint8Array(1);
const typedKeyMaterial: Uint8Array = new Uint8Array(2);

const cipherText = sodium.crypto_stream_chacha20_xor(plainText, typedNonce, typedKeyMaterial, 'uint8array');

const OPSLIMIT = 4;
const MEMLIMIT = 33554432;
const ALG = 1;
let hash: Uint8Array = sodium.crypto_pwhash(sodium.crypto_box_SEEDBYTES, password, salt, OPSLIMIT, MEMLIMIT, ALG);
hash = sodium.crypto_pwhash(sodium.crypto_box_SEEDBYTES, password, salt, OPSLIMIT, MEMLIMIT, ALG, 'uint8array');
const hashString: string = sodium.crypto_pwhash(sodium.crypto_box_SEEDBYTES, password, salt, OPSLIMIT, MEMLIMIT, ALG, 'text');
sodium.randombytes_buf(sodium.crypto_pwhash_SALTBYTES);